# tinycore

Nomadic ultra small graphical desktop operating system. https://hub.docker.com/r/tatsushid/tinycore/

# Website
* http://tinycorelinux.net
* http://distro.ibiblio.org/tinycorelinux/

# Distributed as
* Docker image
* Live CD (isolinux)